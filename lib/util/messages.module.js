"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.messages = void 0;
exports.messages = {
    error: {
        idNotFound: function (id) { return "Provided id [" + id + "] was not found"; }
    }
};
exports.default = exports.messages;
