export declare const messages: {
    error: {
        idNotFound: (id: number) => string;
    };
};
export default messages;
