export declare class Digikam {
    private dbPath;
    private database;
    constructor(dbPath: string);
    get AlbumRoot(): {
        new (obj: import("./models/AlbumRoot.class").AlbumRootData): {
            readonly id: number;
            readonly label: string;
            readonly status: number;
            readonly type: number;
            readonly identifier: string;
            readonly specificPath: string;
            raw: import("./models/AlbumRoot.class").AlbumRootData;
        };
        getAll(): {
            readonly id: number;
            readonly label: string;
            readonly status: number;
            readonly type: number;
            readonly identifier: string;
            readonly specificPath: string;
            raw: import("./models/AlbumRoot.class").AlbumRootData;
        }[];
        get(id: number): {
            readonly id: number;
            readonly label: string;
            readonly status: number;
            readonly type: number;
            readonly identifier: string;
            readonly specificPath: string;
            raw: import("./models/AlbumRoot.class").AlbumRootData;
        };
    };
    get Image(): {
        new (obj: import("./models/Image.class").ImageData): {
            readonly id: number;
            readonly album: number;
            readonly name: string;
            readonly status: number;
            readonly category: number;
            readonly modificationDate: string;
            readonly fileSize: number;
            readonly uniqueHash: string;
            readonly manualOrder: number;
            readonly comments: {
                readonly id: number;
                readonly type: number;
                readonly language: string;
                readonly author: string;
                readonly date: string;
                readonly comment: string;
                readonly imageID: number;
                raw: import("./models/image/ImageComment.class").ImageCommentData;
            }[];
            readonly copyrights: {
                readonly id: number;
                readonly property: string;
                readonly value: string;
                readonly extraValue: string;
                readonly imageID: number;
                raw: import("./models/image/ImageCopyright.class").ImageCopyrightData;
            }[];
            readonly history: {
                readonly uuid: string;
                readonly history: string;
                readonly imageID: number;
                raw: import("./models/image/ImageHistory.class").ImageHistoryData;
            };
            readonly information: {
                readonly rating: number;
                readonly creationDate: string;
                readonly digitizationDate: string;
                readonly orientation: number;
                readonly width: number;
                readonly height: number;
                readonly format: string;
                readonly colorDepth: number;
                readonly colorModel: number;
                readonly imageID: number;
                raw: import("./models/image/ImageInformation.class").ImageInformationData;
            };
            readonly metadata: {
                readonly make: string;
                readonly model: string;
                readonly lens: string;
                readonly aperture: number;
                readonly focalLength: number;
                readonly focalLength35: number;
                readonly exposureTime: number;
                readonly exposureProgram: number;
                readonly exposureMode: number;
                readonly sensitivity: number;
                readonly flash: number;
                readonly whiteBalance: number;
                readonly whiteBalanceColorTemperature: number;
                readonly meteringMode: number;
                readonly subjectDistance: number;
                readonly subjectDistanceCategory: number;
                readonly imageID: number;
                raw: import("./models/image/ImageMetadata.class").ImageMetadataData;
            };
            readonly position: {
                readonly latitude: string;
                readonly latitudeNumber: number;
                readonly longitude: string;
                readonly longitudeNumber: number;
                readonly altitude: number;
                readonly orientation: number;
                readonly tilt: number;
                readonly roll: number;
                readonly accuracy: number;
                readonly description: string;
                readonly imageID: number;
                raw: import("./models/image/ImagePosition.class").ImagePositionData;
            };
            readonly properties: {
                readonly property: string;
                readonly value: any;
                readonly imageID: number;
                raw: import("./models/image/ImageProperty.class").ImagePropertyData;
            }[];
            readonly relations: void;
            readonly tags: {
                readonly id: number;
                readonly pid: number;
                readonly name: string;
                readonly icon: number;
                readonly iconkde: number;
                raw: import("./models/Tag.class").TagData;
            }[];
            raw: import("./models/Image.class").ImageData;
        };
        all(): {
            readonly id: number;
            readonly album: number;
            readonly name: string;
            readonly status: number;
            readonly category: number;
            readonly modificationDate: string;
            readonly fileSize: number;
            readonly uniqueHash: string;
            readonly manualOrder: number;
            readonly comments: {
                readonly id: number;
                readonly type: number;
                readonly language: string;
                readonly author: string;
                readonly date: string;
                readonly comment: string;
                readonly imageID: number;
                raw: import("./models/image/ImageComment.class").ImageCommentData;
            }[];
            readonly copyrights: {
                readonly id: number;
                readonly property: string;
                readonly value: string;
                readonly extraValue: string;
                readonly imageID: number;
                raw: import("./models/image/ImageCopyright.class").ImageCopyrightData;
            }[];
            readonly history: {
                readonly uuid: string;
                readonly history: string;
                readonly imageID: number;
                raw: import("./models/image/ImageHistory.class").ImageHistoryData;
            };
            readonly information: {
                readonly rating: number;
                readonly creationDate: string;
                readonly digitizationDate: string;
                readonly orientation: number;
                readonly width: number;
                readonly height: number;
                readonly format: string;
                readonly colorDepth: number;
                readonly colorModel: number;
                readonly imageID: number;
                raw: import("./models/image/ImageInformation.class").ImageInformationData;
            };
            readonly metadata: {
                readonly make: string;
                readonly model: string;
                readonly lens: string;
                readonly aperture: number;
                readonly focalLength: number;
                readonly focalLength35: number;
                readonly exposureTime: number;
                readonly exposureProgram: number;
                readonly exposureMode: number;
                readonly sensitivity: number;
                readonly flash: number;
                readonly whiteBalance: number;
                readonly whiteBalanceColorTemperature: number;
                readonly meteringMode: number;
                readonly subjectDistance: number;
                readonly subjectDistanceCategory: number;
                readonly imageID: number;
                raw: import("./models/image/ImageMetadata.class").ImageMetadataData;
            };
            readonly position: {
                readonly latitude: string;
                readonly latitudeNumber: number;
                readonly longitude: string;
                readonly longitudeNumber: number;
                readonly altitude: number;
                readonly orientation: number;
                readonly tilt: number;
                readonly roll: number;
                readonly accuracy: number;
                readonly description: string;
                readonly imageID: number;
                raw: import("./models/image/ImagePosition.class").ImagePositionData;
            };
            readonly properties: {
                readonly property: string;
                readonly value: any;
                readonly imageID: number;
                raw: import("./models/image/ImageProperty.class").ImagePropertyData;
            }[];
            readonly relations: void;
            readonly tags: {
                readonly id: number;
                readonly pid: number;
                readonly name: string;
                readonly icon: number;
                readonly iconkde: number;
                raw: import("./models/Tag.class").TagData;
            }[];
            raw: import("./models/Image.class").ImageData;
        }[];
        one(id: number): {
            readonly id: number;
            readonly album: number;
            readonly name: string;
            readonly status: number;
            readonly category: number;
            readonly modificationDate: string;
            readonly fileSize: number;
            readonly uniqueHash: string;
            readonly manualOrder: number;
            readonly comments: {
                readonly id: number;
                readonly type: number;
                readonly language: string;
                readonly author: string;
                readonly date: string;
                readonly comment: string;
                readonly imageID: number;
                raw: import("./models/image/ImageComment.class").ImageCommentData;
            }[];
            readonly copyrights: {
                readonly id: number;
                readonly property: string;
                readonly value: string;
                readonly extraValue: string;
                readonly imageID: number;
                raw: import("./models/image/ImageCopyright.class").ImageCopyrightData;
            }[];
            readonly history: {
                readonly uuid: string;
                readonly history: string;
                readonly imageID: number;
                raw: import("./models/image/ImageHistory.class").ImageHistoryData;
            };
            readonly information: {
                readonly rating: number;
                readonly creationDate: string;
                readonly digitizationDate: string;
                readonly orientation: number;
                readonly width: number;
                readonly height: number;
                readonly format: string;
                readonly colorDepth: number;
                readonly colorModel: number;
                readonly imageID: number;
                raw: import("./models/image/ImageInformation.class").ImageInformationData;
            };
            readonly metadata: {
                readonly make: string;
                readonly model: string;
                readonly lens: string;
                readonly aperture: number;
                readonly focalLength: number;
                readonly focalLength35: number;
                readonly exposureTime: number;
                readonly exposureProgram: number;
                readonly exposureMode: number;
                readonly sensitivity: number;
                readonly flash: number;
                readonly whiteBalance: number;
                readonly whiteBalanceColorTemperature: number;
                readonly meteringMode: number;
                readonly subjectDistance: number;
                readonly subjectDistanceCategory: number;
                readonly imageID: number;
                raw: import("./models/image/ImageMetadata.class").ImageMetadataData;
            };
            readonly position: {
                readonly latitude: string;
                readonly latitudeNumber: number;
                readonly longitude: string;
                readonly longitudeNumber: number;
                readonly altitude: number;
                readonly orientation: number;
                readonly tilt: number;
                readonly roll: number;
                readonly accuracy: number;
                readonly description: string;
                readonly imageID: number;
                raw: import("./models/image/ImagePosition.class").ImagePositionData;
            };
            readonly properties: {
                readonly property: string;
                readonly value: any;
                readonly imageID: number;
                raw: import("./models/image/ImageProperty.class").ImagePropertyData;
            }[];
            readonly relations: void;
            readonly tags: {
                readonly id: number;
                readonly pid: number;
                readonly name: string;
                readonly icon: number;
                readonly iconkde: number;
                raw: import("./models/Tag.class").TagData;
            }[];
            raw: import("./models/Image.class").ImageData;
        };
        getByTag(tagids: number[]): {
            readonly id: number;
            readonly album: number;
            readonly name: string;
            readonly status: number;
            readonly category: number;
            readonly modificationDate: string;
            readonly fileSize: number;
            readonly uniqueHash: string;
            readonly manualOrder: number;
            readonly comments: {
                readonly id: number;
                readonly type: number;
                readonly language: string;
                readonly author: string;
                readonly date: string;
                readonly comment: string;
                readonly imageID: number;
                raw: import("./models/image/ImageComment.class").ImageCommentData;
            }[];
            readonly copyrights: {
                readonly id: number;
                readonly property: string;
                readonly value: string;
                readonly extraValue: string;
                readonly imageID: number;
                raw: import("./models/image/ImageCopyright.class").ImageCopyrightData;
            }[];
            readonly history: {
                readonly uuid: string;
                readonly history: string;
                readonly imageID: number;
                raw: import("./models/image/ImageHistory.class").ImageHistoryData;
            };
            readonly information: {
                readonly rating: number;
                readonly creationDate: string;
                readonly digitizationDate: string;
                readonly orientation: number;
                readonly width: number;
                readonly height: number;
                readonly format: string;
                readonly colorDepth: number;
                readonly colorModel: number;
                readonly imageID: number;
                raw: import("./models/image/ImageInformation.class").ImageInformationData;
            };
            readonly metadata: {
                readonly make: string;
                readonly model: string;
                readonly lens: string;
                readonly aperture: number;
                readonly focalLength: number;
                readonly focalLength35: number;
                readonly exposureTime: number;
                readonly exposureProgram: number;
                readonly exposureMode: number;
                readonly sensitivity: number;
                readonly flash: number;
                readonly whiteBalance: number;
                readonly whiteBalanceColorTemperature: number;
                readonly meteringMode: number;
                readonly subjectDistance: number;
                readonly subjectDistanceCategory: number;
                readonly imageID: number;
                raw: import("./models/image/ImageMetadata.class").ImageMetadataData;
            };
            readonly position: {
                readonly latitude: string;
                readonly latitudeNumber: number;
                readonly longitude: string;
                readonly longitudeNumber: number;
                readonly altitude: number;
                readonly orientation: number;
                readonly tilt: number;
                readonly roll: number;
                readonly accuracy: number;
                readonly description: string;
                readonly imageID: number;
                raw: import("./models/image/ImagePosition.class").ImagePositionData;
            };
            readonly properties: {
                readonly property: string;
                readonly value: any;
                readonly imageID: number;
                raw: import("./models/image/ImageProperty.class").ImagePropertyData;
            }[];
            readonly relations: void;
            readonly tags: {
                readonly id: number;
                readonly pid: number;
                readonly name: string;
                readonly icon: number;
                readonly iconkde: number;
                raw: import("./models/Tag.class").TagData;
            }[];
            raw: import("./models/Image.class").ImageData;
        }[];
    };
}
export default Digikam;
