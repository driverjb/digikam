"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Digikam = void 0;
var better_sqlite3_1 = __importDefault(require("better-sqlite3"));
var AlbumRoot_class_1 = __importDefault(require("./models/AlbumRoot.class"));
var Image_class_1 = __importDefault(require("./models/Image.class"));
var Digikam = /** @class */ (function () {
    function Digikam(dbPath) {
        this.dbPath = dbPath;
        this.database = better_sqlite3_1.default(this.dbPath);
    }
    Object.defineProperty(Digikam.prototype, "AlbumRoot", {
        get: function () {
            return AlbumRoot_class_1.default(this.database);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Digikam.prototype, "Image", {
        get: function () {
            return Image_class_1.default(this.database);
        },
        enumerable: false,
        configurable: true
    });
    return Digikam;
}());
exports.Digikam = Digikam;
exports.default = Digikam;
