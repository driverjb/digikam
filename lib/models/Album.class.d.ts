import { Database } from 'better-sqlite3';
export interface AlbumData {
    id: number;
    albumRoot: number;
    relativePath: string;
    date: string;
    caption: string;
    collection: string;
    icon: number;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildAlbum(db: Database): {
    new (obj: AlbumData): {
        readonly id: number;
        readonly albumRoot: number;
        readonly relativePath: string;
        readonly date: string;
        readonly caption: string;
        readonly collection: string;
        readonly icon: number;
        raw: AlbumData;
    };
    getAll(): {
        readonly id: number;
        readonly albumRoot: number;
        readonly relativePath: string;
        readonly date: string;
        readonly caption: string;
        readonly collection: string;
        readonly icon: number;
        raw: AlbumData;
    }[];
    get(id: number): {
        readonly id: number;
        readonly albumRoot: number;
        readonly relativePath: string;
        readonly date: string;
        readonly caption: string;
        readonly collection: string;
        readonly icon: number;
        raw: AlbumData;
    };
};
export default BuildAlbum;
