import { ObjectSchema } from 'joi';
export declare abstract class ModelBase<T> {
    raw: T;
    constructor(obj: T, schema: ObjectSchema<T>);
}
export default ModelBase;
