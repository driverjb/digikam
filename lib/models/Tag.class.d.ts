import { Database } from 'better-sqlite3';
export interface TagData {
    id: number;
    pid: number;
    name: string;
    icon: number;
    iconkde: number;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildTag(db: Database): {
    new (obj: TagData): {
        readonly id: number;
        readonly pid: number;
        readonly name: string;
        readonly icon: number;
        readonly iconkde: number;
        raw: TagData;
    };
    all(): {
        readonly id: number;
        readonly pid: number;
        readonly name: string;
        readonly icon: number;
        readonly iconkde: number;
        raw: TagData;
    }[];
    one(id: number): {
        readonly id: number;
        readonly pid: number;
        readonly name: string;
        readonly icon: number;
        readonly iconkde: number;
        raw: TagData;
    };
};
export default BuildTag;
