"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImage = void 0;
var chai_1 = require("chai");
var joi_1 = __importDefault(require("joi"));
var messages_module_1 = __importDefault(require("../util/messages.module"));
var ImageComment_class_1 = __importDefault(require("./image/ImageComment.class"));
var ImageCopyright_class_1 = __importDefault(require("./image/ImageCopyright.class"));
var ImageHistory_class_1 = __importDefault(require("./image/ImageHistory.class"));
var ImageInformation_class_1 = __importDefault(require("./image/ImageInformation.class"));
var ImageMetadata_class_1 = __importDefault(require("./image/ImageMetadata.class"));
var ImagePosition_class_1 = __importDefault(require("./image/ImagePosition.class"));
var ImageProperty_class_1 = __importDefault(require("./image/ImageProperty.class"));
var ImageTag_class_1 = __importDefault(require("./image/ImageTag.class"));
var ImageTagProperty_class_1 = __importDefault(require("./image/ImageTagProperty.class"));
var ModelBase_class_1 = __importDefault(require("./ModelBase.class"));
var Tag_class_1 = __importDefault(require("./Tag.class"));
var sql = {
    all: 'select * from Images',
    one: 'select * from Images where id = ?'
};
var schema = joi_1.default.object({
    id: joi_1.default.number().min(1).required(),
    album: joi_1.default.number().min(1).allow(null).default(null),
    name: joi_1.default.string().required(),
    status: joi_1.default.number().min(1).required(),
    category: joi_1.default.number().min(1).required(),
    modificationDate: joi_1.default.string().required(),
    fileSize: joi_1.default.number().min(1).required(),
    uniqueHash: joi_1.default.string().required(),
    manualOrder: joi_1.default.number().allow(null).default(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImage(db) {
    var ImageComment = ImageComment_class_1.default(db);
    var ImageCopyright = ImageCopyright_class_1.default(db);
    var ImageHistory = ImageHistory_class_1.default(db);
    var ImageInformation = ImageInformation_class_1.default(db);
    var ImageMetadata = ImageMetadata_class_1.default(db);
    var ImagePosition = ImagePosition_class_1.default(db);
    var ImageProperty = ImageProperty_class_1.default(db);
    var ImageTag = ImageTag_class_1.default(db);
    var ImageTagProperty = ImageTagProperty_class_1.default(db);
    var Tag = Tag_class_1.default(db);
    return /** @class */ (function (_super) {
        __extends(Image, _super);
        function Image(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(Image.prototype, "id", {
            get: function () {
                return this.raw.id;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "album", {
            get: function () {
                return this.raw.album;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "name", {
            get: function () {
                return this.raw.name;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "status", {
            get: function () {
                return this.raw.status;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "category", {
            get: function () {
                return this.raw.category;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "modificationDate", {
            get: function () {
                return this.raw.modificationDate;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "fileSize", {
            get: function () {
                return this.raw.fileSize;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "uniqueHash", {
            get: function () {
                return this.raw.uniqueHash;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "manualOrder", {
            get: function () {
                return this.raw.manualOrder;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "comments", {
            get: function () {
                return ImageComment.getByImage(this.id);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "copyrights", {
            get: function () {
                return ImageCopyright.getByImage(this.id);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "history", {
            get: function () {
                return ImageHistory.get(this.id);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "information", {
            get: function () {
                return ImageInformation.get(this.id);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "metadata", {
            get: function () {
                return ImageMetadata.get(this.id);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "position", {
            get: function () {
                return ImagePosition.get(this.id);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "properties", {
            get: function () {
                return ImageProperty.getByImage(this.id);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "relations", {
            get: function () {
                throw new Error('Relations not implemented yet');
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Image.prototype, "tags", {
            get: function () {
                console.log(ImageTag.getByImage(this.id));
                return ImageTag.getByImage(this.id).map(function (tags) { return Tag.one(tags.tagid); });
            },
            enumerable: false,
            configurable: true
        });
        Image.all = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new Image(result); });
        };
        Image.one = function (id) {
            var result = db.prepare(sql.one).get(id);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(id)).to.exist;
            return new Image(result);
        };
        Image.getByTag = function (tagids) {
            return tagids.map(function (id) { return ImageTag.getByTag(id).map(function (it) { return Image.one(it.imageID); }); })[0];
        };
        return Image;
    }(ModelBase_class_1.default));
}
exports.BuildImage = BuildImage;
exports.default = BuildImage;
