import Joi from 'joi';
import ModelBase from '../ModelBase.class';
export interface ImageChildBaseData {
    imageid: number;
}
export declare abstract class ImageChildBase<T extends ImageChildBaseData> extends ModelBase<T> {
    constructor(obj: T, schema: Joi.ObjectSchema<T>);
    get imageID(): number;
}
