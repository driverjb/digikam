"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImageMetadata = void 0;
var chai_1 = require("chai");
var joi_1 = __importDefault(require("joi"));
var messages_module_1 = __importDefault(require("../../util/messages.module"));
var ImageChildBase_interface_1 = require("./ImageChildBase.interface");
var sql = {
    all: 'select * from ImageMetadata',
    one: 'select * from ImageMetadata where imageid = ?'
};
var schema = joi_1.default.object({
    make: joi_1.default.string().allow(null).default(null),
    model: joi_1.default.string().allow(null).default(null),
    lens: joi_1.default.string().allow(null).default(null),
    aperture: joi_1.default.number().allow(null).default(null),
    focalLength: joi_1.default.number().allow(null).default(null),
    focalLength35: joi_1.default.number().allow(null).default(null),
    exposureTime: joi_1.default.number().allow(null).default(null),
    exposureProgram: joi_1.default.number().allow(null).default(null),
    exposureMode: joi_1.default.number().allow(null).default(null),
    sensitivity: joi_1.default.number().allow(null).default(null),
    flash: joi_1.default.number().allow(null).default(null),
    whiteBalance: joi_1.default.number().allow(null).default(null),
    whiteBalanceColorTemperature: joi_1.default.number().allow(null).default(null),
    meteringMode: joi_1.default.number().allow(null).default(null),
    subjectDistance: joi_1.default.number().allow(null).default(null),
    subjectDistanceCategory: joi_1.default.number().allow(null).default(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImageMetadata(db) {
    return /** @class */ (function (_super) {
        __extends(ImageMetadata, _super);
        function ImageMetadata(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(ImageMetadata.prototype, "make", {
            get: function () {
                return this.raw.make;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "model", {
            get: function () {
                return this.raw.model;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "lens", {
            get: function () {
                return this.raw.lens;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "aperture", {
            get: function () {
                return this.raw.aperture;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "focalLength", {
            get: function () {
                return this.raw.focalLength;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "focalLength35", {
            get: function () {
                return this.raw.focalLength35;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "exposureTime", {
            get: function () {
                return this.raw.exposureTime;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "exposureProgram", {
            get: function () {
                return this.raw.exposureProgram;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "exposureMode", {
            get: function () {
                return this.raw.exposureMode;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "sensitivity", {
            get: function () {
                return this.raw.sensitivity;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "flash", {
            get: function () {
                return this.raw.flash;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "whiteBalance", {
            get: function () {
                return this.raw.whiteBalance;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "whiteBalanceColorTemperature", {
            get: function () {
                return this.raw.whiteBalanceColorTemperature;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "meteringMode", {
            get: function () {
                return this.raw.meteringMode;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "subjectDistance", {
            get: function () {
                return this.raw.subjectDistance;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageMetadata.prototype, "subjectDistanceCategory", {
            get: function () {
                return this.raw.subjectDistanceCategory;
            },
            enumerable: false,
            configurable: true
        });
        ImageMetadata.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new ImageMetadata(result); });
        };
        ImageMetadata.get = function (imageid) {
            var result = db.prepare(sql.one).get(imageid);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(imageid)).to.exist;
            return new ImageMetadata(result);
        };
        return ImageMetadata;
    }(ImageChildBase_interface_1.ImageChildBase));
}
exports.BuildImageMetadata = BuildImageMetadata;
exports.default = BuildImageMetadata;
