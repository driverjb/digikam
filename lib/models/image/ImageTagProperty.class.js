"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImageTagProperty = void 0;
var chai_1 = require("chai");
var joi_1 = __importDefault(require("joi"));
var messages_module_1 = __importDefault(require("../../util/messages.module"));
var ImageChildBase_interface_1 = require("./ImageChildBase.interface");
var sql = {
    all: 'select * from ImageTagProperties',
    one: 'select * from ImageTagProperties where tagid = ?',
    byImage: 'select * from ImageTagProperties where imageid = ?'
};
var schema = joi_1.default.object({
    tagid: joi_1.default.number().min(1).required(),
    property: joi_1.default.string().required(),
    value: joi_1.default.any().allow(null).default(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImageTagProperty(db) {
    return /** @class */ (function (_super) {
        __extends(ImageTagProperty, _super);
        function ImageTagProperty(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(ImageTagProperty.prototype, "tagid", {
            get: function () {
                return this.raw.tagid;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageTagProperty.prototype, "property", {
            get: function () {
                return this.raw.property;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageTagProperty.prototype, "value", {
            get: function () {
                return this.raw.value;
            },
            enumerable: false,
            configurable: true
        });
        ImageTagProperty.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new ImageTagProperty(result); });
        };
        ImageTagProperty.get = function (tagid) {
            var result = db.prepare(sql.one).get(tagid);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(tagid)).to.exist;
            return new ImageTagProperty(result);
        };
        ImageTagProperty.getByImage = function (imageid) {
            return db
                .prepare(sql.all)
                .all(imageid)
                .map(function (result) { return new ImageTagProperty(result); });
        };
        return ImageTagProperty;
    }(ImageChildBase_interface_1.ImageChildBase));
}
exports.BuildImageTagProperty = BuildImageTagProperty;
exports.default = BuildImageTagProperty;
