"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImageTag = void 0;
var joi_1 = __importDefault(require("joi"));
var ImageChildBase_interface_1 = require("./ImageChildBase.interface");
var sql = {
    all: 'select * from ImageTags',
    byTag: 'select * from ImageTags where tagid = ?',
    byImage: 'select * from ImageTags where imageid = ?'
};
var schema = joi_1.default.object({
    tagid: joi_1.default.number().min(1).required()
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImageTag(db) {
    return /** @class */ (function (_super) {
        __extends(ImageTag, _super);
        function ImageTag(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(ImageTag.prototype, "tagid", {
            get: function () {
                return this.raw.tagid;
            },
            enumerable: false,
            configurable: true
        });
        ImageTag.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new ImageTag(result); });
        };
        ImageTag.getByTag = function (tagid) {
            return db
                .prepare(sql.byTag)
                .all(tagid)
                .map(function (result) { return new ImageTag(result); });
        };
        ImageTag.getByImage = function (imageid) {
            return db
                .prepare(sql.byImage)
                .all(imageid)
                .map(function (result) { return new ImageTag(result); });
        };
        return ImageTag;
    }(ImageChildBase_interface_1.ImageChildBase));
}
exports.BuildImageTag = BuildImageTag;
exports.default = BuildImageTag;
//get $1(){ return this.raw.$1 }
