import { Database } from 'better-sqlite3';
import { ImageChildBaseData } from './ImageChildBase.interface';
export interface ImageRelationData extends ImageChildBaseData {
    subject: number;
    object: number;
    type: number;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildImageRelation(db: Database): {
    new (obj: ImageRelationData): {
        readonly subject: number;
        readonly object: number;
        readonly type: number;
        readonly imageID: number;
        raw: ImageRelationData;
    };
    getAll(): {
        readonly subject: number;
        readonly object: number;
        readonly type: number;
        readonly imageID: number;
        raw: ImageRelationData;
    }[];
};
export default BuildImageRelation;
