import { Database } from 'better-sqlite3';
import { ImageChildBaseData } from './ImageChildBase.interface';
export interface ImageHistoryData extends ImageChildBaseData {
    uuid: string;
    history: string;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildImageHistory(db: Database): {
    new (obj: ImageHistoryData): {
        readonly uuid: string;
        readonly history: string;
        readonly imageID: number;
        raw: ImageHistoryData;
    };
    getAll(): {
        readonly uuid: string;
        readonly history: string;
        readonly imageID: number;
        raw: ImageHistoryData;
    }[];
    get(imageID: number): {
        readonly uuid: string;
        readonly history: string;
        readonly imageID: number;
        raw: ImageHistoryData;
    };
};
export default BuildImageHistory;
