import { Database } from 'better-sqlite3';
import { ImageChildBaseData } from './ImageChildBase.interface';
export interface ImageMetadataData extends ImageChildBaseData {
    make: string;
    model: string;
    lens: string;
    aperture: number;
    focalLength: number;
    focalLength35: number;
    exposureTime: number;
    exposureProgram: number;
    exposureMode: number;
    sensitivity: number;
    flash: number;
    whiteBalance: number;
    whiteBalanceColorTemperature: number;
    meteringMode: number;
    subjectDistance: number;
    subjectDistanceCategory: number;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildImageMetadata(db: Database): {
    new (obj: ImageMetadataData): {
        readonly make: string;
        readonly model: string;
        readonly lens: string;
        readonly aperture: number;
        readonly focalLength: number;
        readonly focalLength35: number;
        readonly exposureTime: number;
        readonly exposureProgram: number;
        readonly exposureMode: number;
        readonly sensitivity: number;
        readonly flash: number;
        readonly whiteBalance: number;
        readonly whiteBalanceColorTemperature: number;
        readonly meteringMode: number;
        readonly subjectDistance: number;
        readonly subjectDistanceCategory: number;
        readonly imageID: number;
        raw: ImageMetadataData;
    };
    getAll(): {
        readonly make: string;
        readonly model: string;
        readonly lens: string;
        readonly aperture: number;
        readonly focalLength: number;
        readonly focalLength35: number;
        readonly exposureTime: number;
        readonly exposureProgram: number;
        readonly exposureMode: number;
        readonly sensitivity: number;
        readonly flash: number;
        readonly whiteBalance: number;
        readonly whiteBalanceColorTemperature: number;
        readonly meteringMode: number;
        readonly subjectDistance: number;
        readonly subjectDistanceCategory: number;
        readonly imageID: number;
        raw: ImageMetadataData;
    }[];
    get(imageid: number): {
        readonly make: string;
        readonly model: string;
        readonly lens: string;
        readonly aperture: number;
        readonly focalLength: number;
        readonly focalLength35: number;
        readonly exposureTime: number;
        readonly exposureProgram: number;
        readonly exposureMode: number;
        readonly sensitivity: number;
        readonly flash: number;
        readonly whiteBalance: number;
        readonly whiteBalanceColorTemperature: number;
        readonly meteringMode: number;
        readonly subjectDistance: number;
        readonly subjectDistanceCategory: number;
        readonly imageID: number;
        raw: ImageMetadataData;
    };
};
export default BuildImageMetadata;
