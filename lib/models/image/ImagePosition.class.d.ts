import { Database } from 'better-sqlite3';
import { ImageChildBaseData } from './ImageChildBase.interface';
export interface ImagePositionData extends ImageChildBaseData {
    latitude: string;
    latitudeNumber: number;
    longitude: string;
    longitudeNumber: number;
    altitude: number;
    orientation: number;
    tilt: number;
    roll: number;
    accuracy: number;
    description: string;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildImagePosition(db: Database): {
    new (obj: ImagePositionData): {
        readonly latitude: string;
        readonly latitudeNumber: number;
        readonly longitude: string;
        readonly longitudeNumber: number;
        readonly altitude: number;
        readonly orientation: number;
        readonly tilt: number;
        readonly roll: number;
        readonly accuracy: number;
        readonly description: string;
        readonly imageID: number;
        raw: ImagePositionData;
    };
    getAll(): {
        readonly latitude: string;
        readonly latitudeNumber: number;
        readonly longitude: string;
        readonly longitudeNumber: number;
        readonly altitude: number;
        readonly orientation: number;
        readonly tilt: number;
        readonly roll: number;
        readonly accuracy: number;
        readonly description: string;
        readonly imageID: number;
        raw: ImagePositionData;
    }[];
    get(imageid: number): {
        readonly latitude: string;
        readonly latitudeNumber: number;
        readonly longitude: string;
        readonly longitudeNumber: number;
        readonly altitude: number;
        readonly orientation: number;
        readonly tilt: number;
        readonly roll: number;
        readonly accuracy: number;
        readonly description: string;
        readonly imageID: number;
        raw: ImagePositionData;
    };
};
export default BuildImagePosition;
