import { Database } from 'better-sqlite3';
import { ImageChildBaseData } from './ImageChildBase.interface';
export interface ImageTagPropertyData extends ImageChildBaseData {
    tagid: number;
    property: string;
    value: any;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildImageTagProperty(db: Database): {
    new (obj: ImageTagPropertyData): {
        readonly tagid: number;
        readonly property: string;
        readonly value: any;
        readonly imageID: number;
        raw: ImageTagPropertyData;
    };
    getAll(): {
        readonly tagid: number;
        readonly property: string;
        readonly value: any;
        readonly imageID: number;
        raw: ImageTagPropertyData;
    }[];
    get(tagid: number): {
        readonly tagid: number;
        readonly property: string;
        readonly value: any;
        readonly imageID: number;
        raw: ImageTagPropertyData;
    };
    getByImage(imageid: number): {
        readonly tagid: number;
        readonly property: string;
        readonly value: any;
        readonly imageID: number;
        raw: ImageTagPropertyData;
    }[];
};
export default BuildImageTagProperty;
