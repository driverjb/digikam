import { Database } from 'better-sqlite3';
import { ImageChildBaseData } from './ImageChildBase.interface';
export interface ImageCommentData extends ImageChildBaseData {
    id: number;
    type: number;
    language: string;
    author?: string;
    date?: string;
    comment: string;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildImageComment(db: Database): {
    new (obj: ImageCommentData): {
        readonly id: number;
        readonly type: number;
        readonly language: string;
        readonly author: string;
        readonly date: string;
        readonly comment: string;
        readonly imageID: number;
        raw: ImageCommentData;
    };
    getAll(): {
        readonly id: number;
        readonly type: number;
        readonly language: string;
        readonly author: string;
        readonly date: string;
        readonly comment: string;
        readonly imageID: number;
        raw: ImageCommentData;
    }[];
    get(id: number): {
        readonly id: number;
        readonly type: number;
        readonly language: string;
        readonly author: string;
        readonly date: string;
        readonly comment: string;
        readonly imageID: number;
        raw: ImageCommentData;
    };
    getByImage(imageid: number): {
        readonly id: number;
        readonly type: number;
        readonly language: string;
        readonly author: string;
        readonly date: string;
        readonly comment: string;
        readonly imageID: number;
        raw: ImageCommentData;
    }[];
};
export default BuildImageComment;
