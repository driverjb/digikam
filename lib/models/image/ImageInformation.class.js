"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImageInformation = void 0;
var chai_1 = require("chai");
var joi_1 = __importDefault(require("joi"));
var messages_module_1 = __importDefault(require("../../util/messages.module"));
var ImageChildBase_interface_1 = require("./ImageChildBase.interface");
var sql = {
    all: 'select * from ImageInformation',
    one: 'select * from ImageInformation where imageid = ?'
};
var schema = joi_1.default.object({
    rating: joi_1.default.number().min(-1).max(5).required(),
    creationDate: joi_1.default.string().required(),
    digitizationDate: joi_1.default.string().allow(null).default(null),
    orientation: joi_1.default.number().min(0).required(),
    width: joi_1.default.number().min(1).required(),
    height: joi_1.default.number().min(1).required(),
    format: joi_1.default.string().required(),
    colorDepth: joi_1.default.number().min(1).required(),
    colorModel: joi_1.default.number().min(1).required()
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImageInformation(db) {
    return /** @class */ (function (_super) {
        __extends(ImageInformation, _super);
        function ImageInformation(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(ImageInformation.prototype, "rating", {
            get: function () {
                return this.raw.rating;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageInformation.prototype, "creationDate", {
            get: function () {
                return this.raw.creationDate;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageInformation.prototype, "digitizationDate", {
            get: function () {
                return this.raw.digitizationDate;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageInformation.prototype, "orientation", {
            get: function () {
                return this.raw.orientation;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageInformation.prototype, "width", {
            get: function () {
                return this.raw.width;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageInformation.prototype, "height", {
            get: function () {
                return this.raw.height;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageInformation.prototype, "format", {
            get: function () {
                return this.raw.format;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageInformation.prototype, "colorDepth", {
            get: function () {
                return this.raw.colorDepth;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageInformation.prototype, "colorModel", {
            get: function () {
                return this.raw.colorModel;
            },
            enumerable: false,
            configurable: true
        });
        ImageInformation.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new ImageInformation(result); });
        };
        ImageInformation.get = function (imageid) {
            var result = db.prepare(sql.one).get(imageid);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(imageid)).to.exist;
            return new ImageInformation(result);
        };
        return ImageInformation;
    }(ImageChildBase_interface_1.ImageChildBase));
}
exports.BuildImageInformation = BuildImageInformation;
exports.default = BuildImageInformation;
