import { Database } from 'better-sqlite3';
import { ImageChildBaseData } from './ImageChildBase.interface';
export interface ImageCopyrightData extends ImageChildBaseData {
    id: number;
    property: string;
    value: string;
    extraValue: string;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildImageCopyright(db: Database): {
    new (obj: ImageCopyrightData): {
        readonly id: number;
        readonly property: string;
        readonly value: string;
        readonly extraValue: string;
        readonly imageID: number;
        raw: ImageCopyrightData;
    };
    getAll(): {
        readonly id: number;
        readonly property: string;
        readonly value: string;
        readonly extraValue: string;
        readonly imageID: number;
        raw: ImageCopyrightData;
    }[];
    get(id: number): {
        readonly id: number;
        readonly property: string;
        readonly value: string;
        readonly extraValue: string;
        readonly imageID: number;
        raw: ImageCopyrightData;
    };
    getByImage(imageid: number): {
        readonly id: number;
        readonly property: string;
        readonly value: string;
        readonly extraValue: string;
        readonly imageID: number;
        raw: ImageCopyrightData;
    }[];
};
export default BuildImageCopyright;
