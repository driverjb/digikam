"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImageChildBase = void 0;
var joi_1 = __importDefault(require("joi"));
var ModelBase_class_1 = __importDefault(require("../ModelBase.class"));
var imageChildBaseSchema = joi_1.default.object({
    imageid: joi_1.default.number().min(1).required()
});
var ImageChildBase = /** @class */ (function (_super) {
    __extends(ImageChildBase, _super);
    function ImageChildBase(obj, schema) {
        return _super.call(this, obj, schema.concat(imageChildBaseSchema)) || this;
    }
    Object.defineProperty(ImageChildBase.prototype, "imageID", {
        get: function () {
            return this.raw.imageid;
        },
        enumerable: false,
        configurable: true
    });
    return ImageChildBase;
}(ModelBase_class_1.default));
exports.ImageChildBase = ImageChildBase;
