"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImagePosition = void 0;
var chai_1 = require("chai");
var joi_1 = __importDefault(require("joi"));
var messages_module_1 = __importDefault(require("../../util/messages.module"));
var ImageChildBase_interface_1 = require("./ImageChildBase.interface");
var sql = {
    all: 'select * from ImagePositions',
    one: 'select * from ImagePositions where imageid = ?'
};
var schema = joi_1.default.object({
    latitude: joi_1.default.string().allow(null).default(null),
    latitudeNumber: joi_1.default.number().allow(null).default(null),
    longitude: joi_1.default.string().allow(null).default(null),
    longitudeNumber: joi_1.default.number().allow(null).default(null),
    altitude: joi_1.default.number().allow(null).default(null),
    orientation: joi_1.default.number().allow(null).default(null),
    tilt: joi_1.default.number().allow(null).default(null),
    roll: joi_1.default.number().allow(null).default(null),
    accuracy: joi_1.default.number().allow(null).default(null),
    description: joi_1.default.string().allow(null).default(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImagePosition(db) {
    return /** @class */ (function (_super) {
        __extends(ImagePosition, _super);
        function ImagePosition(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(ImagePosition.prototype, "latitude", {
            get: function () {
                return this.raw.latitude;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImagePosition.prototype, "latitudeNumber", {
            get: function () {
                return this.raw.latitudeNumber;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImagePosition.prototype, "longitude", {
            get: function () {
                return this.raw.longitude;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImagePosition.prototype, "longitudeNumber", {
            get: function () {
                return this.raw.longitudeNumber;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImagePosition.prototype, "altitude", {
            get: function () {
                return this.raw.altitude;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImagePosition.prototype, "orientation", {
            get: function () {
                return this.raw.orientation;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImagePosition.prototype, "tilt", {
            get: function () {
                return this.raw.tilt;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImagePosition.prototype, "roll", {
            get: function () {
                return this.raw.roll;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImagePosition.prototype, "accuracy", {
            get: function () {
                return this.raw.accuracy;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImagePosition.prototype, "description", {
            get: function () {
                return this.raw.description;
            },
            enumerable: false,
            configurable: true
        });
        ImagePosition.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new ImagePosition(result); });
        };
        ImagePosition.get = function (imageid) {
            var result = db.prepare(sql.one).get(imageid);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(imageid)).to.exist;
            return new ImagePosition(result);
        };
        return ImagePosition;
    }(ImageChildBase_interface_1.ImageChildBase));
}
exports.BuildImagePosition = BuildImagePosition;
exports.default = BuildImagePosition;
