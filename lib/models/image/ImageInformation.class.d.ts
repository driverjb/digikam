import { Database } from 'better-sqlite3';
import { ImageChildBaseData } from './ImageChildBase.interface';
export interface ImageInformationData extends ImageChildBaseData {
    rating: number;
    creationDate: string;
    digitizationDate: string;
    orientation: number;
    width: number;
    height: number;
    format: string;
    colorDepth: number;
    colorModel: number;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildImageInformation(db: Database): {
    new (obj: ImageInformationData): {
        readonly rating: number;
        readonly creationDate: string;
        readonly digitizationDate: string;
        readonly orientation: number;
        readonly width: number;
        readonly height: number;
        readonly format: string;
        readonly colorDepth: number;
        readonly colorModel: number;
        readonly imageID: number;
        raw: ImageInformationData;
    };
    getAll(): {
        readonly rating: number;
        readonly creationDate: string;
        readonly digitizationDate: string;
        readonly orientation: number;
        readonly width: number;
        readonly height: number;
        readonly format: string;
        readonly colorDepth: number;
        readonly colorModel: number;
        readonly imageID: number;
        raw: ImageInformationData;
    }[];
    get(imageid: number): {
        readonly rating: number;
        readonly creationDate: string;
        readonly digitizationDate: string;
        readonly orientation: number;
        readonly width: number;
        readonly height: number;
        readonly format: string;
        readonly colorDepth: number;
        readonly colorModel: number;
        readonly imageID: number;
        raw: ImageInformationData;
    };
};
export default BuildImageInformation;
