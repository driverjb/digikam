import { Database } from 'better-sqlite3';
import { ImageChildBaseData } from './ImageChildBase.interface';
export interface ImageTagData extends ImageChildBaseData {
    tagid: number;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildImageTag(db: Database): {
    new (obj: ImageTagData): {
        readonly tagid: number;
        readonly imageID: number;
        raw: ImageTagData;
    };
    getAll(): {
        readonly tagid: number;
        readonly imageID: number;
        raw: ImageTagData;
    }[];
    getByTag(tagid: number): {
        readonly tagid: number;
        readonly imageID: number;
        raw: ImageTagData;
    }[];
    getByImage(imageid: number): {
        readonly tagid: number;
        readonly imageID: number;
        raw: ImageTagData;
    }[];
};
export default BuildImageTag;
