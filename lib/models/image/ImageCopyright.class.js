"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImageCopyright = void 0;
var chai_1 = require("chai");
var joi_1 = __importDefault(require("joi"));
var messages_module_1 = __importDefault(require("../../util/messages.module"));
var ImageChildBase_interface_1 = require("./ImageChildBase.interface");
var sql = {
    all: 'select * from ImageCopyright',
    one: 'select * from ImageCopyright where id = ?',
    byImage: 'select * from ImageCopyright where imageid = ?'
};
var schema = joi_1.default.object({
    id: joi_1.default.number().min(1).required(),
    property: joi_1.default.string().required(),
    value: joi_1.default.string().allow(null).default(null),
    extraValue: joi_1.default.string().allow(null).default(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImageCopyright(db) {
    return /** @class */ (function (_super) {
        __extends(ImageCopyright, _super);
        function ImageCopyright(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(ImageCopyright.prototype, "id", {
            get: function () {
                return this.raw.id;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageCopyright.prototype, "property", {
            get: function () {
                return this.raw.property;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageCopyright.prototype, "value", {
            get: function () {
                return this.raw.value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageCopyright.prototype, "extraValue", {
            get: function () {
                return this.raw.extraValue;
            },
            enumerable: false,
            configurable: true
        });
        ImageCopyright.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new ImageCopyright(result); });
        };
        ImageCopyright.get = function (id) {
            var result = db.prepare(sql.one).get(id);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(id)).to.exist;
            return new ImageCopyright(result);
        };
        ImageCopyright.getByImage = function (imageid) {
            return db
                .prepare(sql.byImage)
                .all(imageid)
                .map(function (result) { return new ImageCopyright(result); });
        };
        return ImageCopyright;
    }(ImageChildBase_interface_1.ImageChildBase));
}
exports.BuildImageCopyright = BuildImageCopyright;
exports.default = BuildImageCopyright;
