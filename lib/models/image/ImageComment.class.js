"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImageComment = void 0;
var chai_1 = require("chai");
var joi_1 = __importDefault(require("joi"));
var messages_module_1 = __importDefault(require("../../util/messages.module"));
var ImageChildBase_interface_1 = require("./ImageChildBase.interface");
var sql = {
    all: 'select * from ImageComments',
    one: 'select * from ImageComments where id = ?',
    byImage: 'select * from ImageComments where imageid = ?'
};
var schema = joi_1.default.object({
    id: joi_1.default.number().min(1).required(),
    type: joi_1.default.number().min(1).required(),
    language: joi_1.default.string().required(),
    author: joi_1.default.string().allow(null).default(null),
    date: joi_1.default.string().allow(null).default(null),
    comment: joi_1.default.string().allow(null).default(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImageComment(db) {
    return /** @class */ (function (_super) {
        __extends(ImageComment, _super);
        function ImageComment(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(ImageComment.prototype, "id", {
            get: function () {
                return this.raw.id;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageComment.prototype, "type", {
            get: function () {
                return this.raw.type;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageComment.prototype, "language", {
            get: function () {
                return this.raw.language;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageComment.prototype, "author", {
            get: function () {
                return this.raw.author;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageComment.prototype, "date", {
            get: function () {
                return this.raw.date;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageComment.prototype, "comment", {
            get: function () {
                return this.raw.comment;
            },
            enumerable: false,
            configurable: true
        });
        ImageComment.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new ImageComment(result); });
        };
        ImageComment.get = function (id) {
            var result = db.prepare(sql.one).get(id);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(id)).to.exist;
            return new ImageComment(result);
        };
        ImageComment.getByImage = function (imageid) {
            return db
                .prepare(sql.byImage)
                .all(imageid)
                .map(function (result) { return new ImageComment(result); });
        };
        return ImageComment;
    }(ImageChildBase_interface_1.ImageChildBase));
}
exports.BuildImageComment = BuildImageComment;
exports.default = BuildImageComment;
