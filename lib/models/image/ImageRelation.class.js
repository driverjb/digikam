"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImageRelation = void 0;
var joi_1 = __importDefault(require("joi"));
var ImageChildBase_interface_1 = require("./ImageChildBase.interface");
var sql = {
    all: 'select * from ImageRelations'
};
var schema = joi_1.default.object({
    subject: joi_1.default.number().allow(null),
    object: joi_1.default.number().allow(null),
    type: joi_1.default.number().allow(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImageRelation(db) {
    return /** @class */ (function (_super) {
        __extends(ImageRelation, _super);
        function ImageRelation(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(ImageRelation.prototype, "subject", {
            get: function () {
                return this.raw.subject;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageRelation.prototype, "object", {
            get: function () {
                return this.raw.object;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageRelation.prototype, "type", {
            get: function () {
                return this.raw.type;
            },
            enumerable: false,
            configurable: true
        });
        ImageRelation.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new ImageRelation(result); });
        };
        return ImageRelation;
    }(ImageChildBase_interface_1.ImageChildBase));
}
exports.BuildImageRelation = BuildImageRelation;
exports.default = BuildImageRelation;
