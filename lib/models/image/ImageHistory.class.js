"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImageHistory = void 0;
var chai_1 = require("chai");
var joi_1 = __importDefault(require("joi"));
var messages_module_1 = __importDefault(require("../../util/messages.module"));
var ImageChildBase_interface_1 = require("./ImageChildBase.interface");
var sql = {
    all: 'select * from ImageHistory',
    one: 'select * from ImageHistory where imageid = ?'
};
var schema = joi_1.default.object({
    uuid: joi_1.default.string().required(),
    history: joi_1.default.string().allow(null).default(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImageHistory(db) {
    return /** @class */ (function (_super) {
        __extends(ImageHistory, _super);
        function ImageHistory(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(ImageHistory.prototype, "uuid", {
            get: function () {
                return this.raw.uuid;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageHistory.prototype, "history", {
            get: function () {
                return this.raw.history;
            },
            enumerable: false,
            configurable: true
        });
        ImageHistory.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new ImageHistory(result); });
        };
        ImageHistory.get = function (imageID) {
            var result = db.prepare(sql.one).get(imageID);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(imageID)).to.exist;
            return new ImageHistory(result);
        };
        return ImageHistory;
    }(ImageChildBase_interface_1.ImageChildBase));
}
exports.BuildImageHistory = BuildImageHistory;
exports.default = BuildImageHistory;
