import { Database } from 'better-sqlite3';
import { ImageChildBaseData } from './ImageChildBase.interface';
export interface ImagePropertyData extends ImageChildBaseData {
    property: string;
    value: any;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildImageProperty(db: Database): {
    new (obj: ImagePropertyData): {
        readonly property: string;
        readonly value: any;
        readonly imageID: number;
        raw: ImagePropertyData;
    };
    getAll(): {
        readonly property: string;
        readonly value: any;
        readonly imageID: number;
        raw: ImagePropertyData;
    }[];
    getByImage(imageid: number): {
        readonly property: string;
        readonly value: any;
        readonly imageID: number;
        raw: ImagePropertyData;
    }[];
};
export default BuildImageProperty;
