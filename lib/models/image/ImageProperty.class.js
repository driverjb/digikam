"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildImageProperty = void 0;
var joi_1 = __importDefault(require("joi"));
var ImageChildBase_interface_1 = require("./ImageChildBase.interface");
var sql = {
    all: 'select * from ImageProperties',
    byImage: 'select * from ImageProperties where imageid = ?'
};
var schema = joi_1.default.object({
    property: joi_1.default.string().required(),
    value: joi_1.default.any().allow(null).default(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildImageProperty(db) {
    return /** @class */ (function (_super) {
        __extends(ImageProperty, _super);
        function ImageProperty(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(ImageProperty.prototype, "property", {
            get: function () {
                return this.raw.property;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageProperty.prototype, "value", {
            get: function () {
                return this.raw.value;
            },
            enumerable: false,
            configurable: true
        });
        ImageProperty.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new ImageProperty(result); });
        };
        ImageProperty.getByImage = function (imageid) {
            return db
                .prepare(sql.byImage)
                .all(imageid)
                .map(function (result) { return new ImageProperty(result); });
        };
        return ImageProperty;
    }(ImageChildBase_interface_1.ImageChildBase));
}
exports.BuildImageProperty = BuildImageProperty;
exports.default = BuildImageProperty;
//get $1(){ return this.raw.$1 }
