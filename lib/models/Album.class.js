"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildAlbum = void 0;
var chai_1 = require("chai");
var messages_module_1 = __importDefault(require("../util/messages.module"));
var ModelBase_class_1 = __importDefault(require("./ModelBase.class"));
var joi_1 = __importDefault(require("joi"));
var sql = {
    all: "select * from Albums",
    one: "select * from Albums where id = ?"
};
var schema = joi_1.default.object({
    id: joi_1.default.number().min(1).required(),
    albumRoot: joi_1.default.number().min(1).required(),
    relativePath: joi_1.default.string().required(),
    date: joi_1.default.string().required(),
    caption: joi_1.default.string().allow(null).default(null),
    collection: joi_1.default.string().allow(null).default(null),
    icon: joi_1.default.number().allow(null).default(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildAlbum(db) {
    return /** @class */ (function (_super) {
        __extends(Album, _super);
        function Album(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(Album.prototype, "id", {
            get: function () {
                return this.raw.id;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Album.prototype, "albumRoot", {
            get: function () {
                return this.raw.albumRoot;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Album.prototype, "relativePath", {
            get: function () {
                return this.raw.relativePath;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Album.prototype, "date", {
            get: function () {
                return this.raw.date;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Album.prototype, "caption", {
            get: function () {
                return this.raw.caption;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Album.prototype, "collection", {
            get: function () {
                return this.raw.collection;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Album.prototype, "icon", {
            get: function () {
                return this.raw.icon;
            },
            enumerable: false,
            configurable: true
        });
        Album.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new Album(result); });
        };
        Album.get = function (id) {
            var result = db.prepare(sql.one).get(id);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(id)).to.exist;
            return new Album(result);
        };
        return Album;
    }(ModelBase_class_1.default));
}
exports.BuildAlbum = BuildAlbum;
exports.default = BuildAlbum;
