"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildTag = void 0;
var chai_1 = require("chai");
var joi_1 = __importDefault(require("joi"));
var messages_module_1 = __importDefault(require("../util/messages.module"));
var ModelBase_class_1 = __importDefault(require("./ModelBase.class"));
var sql = {
    all: 'select * from Tags',
    one: 'select * from Tags where id = ?'
};
var schema = joi_1.default.object({
    id: joi_1.default.number().min(1).required(),
    pid: joi_1.default.number().min(0).required(),
    name: joi_1.default.string().required(),
    icon: joi_1.default.number().allow(null).default(null),
    iconkde: joi_1.default.string().allow(null).default(null)
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildTag(db) {
    return /** @class */ (function (_super) {
        __extends(Tag, _super);
        function Tag(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(Tag.prototype, "id", {
            get: function () {
                return this.raw.id;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Tag.prototype, "pid", {
            get: function () {
                return this.raw.pid;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Tag.prototype, "name", {
            get: function () {
                return this.raw.name;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Tag.prototype, "icon", {
            get: function () {
                return this.raw.icon;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Tag.prototype, "iconkde", {
            get: function () {
                return this.raw.iconkde;
            },
            enumerable: false,
            configurable: true
        });
        Tag.all = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new Tag(result); });
        };
        Tag.one = function (id) {
            var result = db.prepare(sql.one).get(id);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(id)).to.exist;
            return new Tag(result);
        };
        return Tag;
    }(ModelBase_class_1.default));
}
exports.BuildTag = BuildTag;
exports.default = BuildTag;
