"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModelBase = void 0;
var ModelBase = /** @class */ (function () {
    function ModelBase(obj, schema) {
        var _a = schema.validate(obj), error = _a.error, value = _a.value;
        if (error)
            throw error;
        this.raw = value;
    }
    return ModelBase;
}());
exports.ModelBase = ModelBase;
exports.default = ModelBase;
