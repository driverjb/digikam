import { Database } from 'better-sqlite3';
export interface AlbumRootData {
    id: number;
    label: string;
    status: number;
    type: number;
    identifier: string;
    specificPath: string;
}
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export declare function BuildAlbumRoot(db: Database): {
    new (obj: AlbumRootData): {
        readonly id: number;
        readonly label: string;
        readonly status: number;
        readonly type: number;
        readonly identifier: string;
        readonly specificPath: string;
        raw: AlbumRootData;
    };
    getAll(): {
        readonly id: number;
        readonly label: string;
        readonly status: number;
        readonly type: number;
        readonly identifier: string;
        readonly specificPath: string;
        raw: AlbumRootData;
    }[];
    get(id: number): {
        readonly id: number;
        readonly label: string;
        readonly status: number;
        readonly type: number;
        readonly identifier: string;
        readonly specificPath: string;
        raw: AlbumRootData;
    };
};
export default BuildAlbumRoot;
