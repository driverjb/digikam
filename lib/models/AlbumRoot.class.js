"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuildAlbumRoot = void 0;
var chai_1 = require("chai");
var messages_module_1 = __importDefault(require("../util/messages.module"));
var ModelBase_class_1 = __importDefault(require("./ModelBase.class"));
var joi_1 = __importDefault(require("joi"));
var sql = {
    all: "select * from AlbumRoots",
    one: "select * from AlbumRoots where id = ?"
};
var schema = joi_1.default.object({
    id: joi_1.default.number().required().min(1),
    label: joi_1.default.string().required(),
    status: joi_1.default.number().min(0).required(),
    type: joi_1.default.number().min(0).required(),
    identifier: joi_1.default.string().required(),
    specificPath: joi_1.default.string().required()
});
/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
function BuildAlbumRoot(db) {
    return /** @class */ (function (_super) {
        __extends(AlbumRoot, _super);
        function AlbumRoot(obj) {
            return _super.call(this, obj, schema) || this;
        }
        Object.defineProperty(AlbumRoot.prototype, "id", {
            get: function () {
                return this.raw.id;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(AlbumRoot.prototype, "label", {
            get: function () {
                return this.raw.label;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(AlbumRoot.prototype, "status", {
            get: function () {
                return this.raw.status;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(AlbumRoot.prototype, "type", {
            get: function () {
                return this.raw.type;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(AlbumRoot.prototype, "identifier", {
            get: function () {
                return this.raw.identifier;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(AlbumRoot.prototype, "specificPath", {
            get: function () {
                return this.raw.specificPath;
            },
            enumerable: false,
            configurable: true
        });
        AlbumRoot.getAll = function () {
            return db
                .prepare(sql.all)
                .all()
                .map(function (result) { return new AlbumRoot(result); });
        };
        AlbumRoot.get = function (id) {
            var result = db.prepare(sql.one).get(id);
            chai_1.expect(result, messages_module_1.default.error.idNotFound(id)).to.exist;
            return new AlbumRoot(result);
        };
        return AlbumRoot;
    }(ModelBase_class_1.default));
}
exports.BuildAlbumRoot = BuildAlbumRoot;
exports.default = BuildAlbumRoot;
