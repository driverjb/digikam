import sqlite3 from 'better-sqlite3';
import BuildAlbumRoot from './models/AlbumRoot.class';
import BuildImage from './models/Image.class';

export class Digikam {
  private dbPath: string;
  private database: sqlite3.Database;
  constructor(dbPath: string) {
    this.dbPath = dbPath;
    this.database = sqlite3(this.dbPath);
  }
  get AlbumRoot() {
    return BuildAlbumRoot(this.database);
  }
  get Image() {
    return BuildImage(this.database);
  }
}

export default Digikam;
