import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../util/messages.module';
import ModelBase from './ModelBase.class';

const sql = {
  all: 'select * from Tags',
  one: 'select * from Tags where id = ?'
};

export interface TagData {
  id: number;
  pid: number;
  name: string;
  icon: number;
  iconkde: number;
}

const schema = Joi.object<TagData>({
  id: Joi.number().min(1).required(),
  pid: Joi.number().min(0).required(),
  name: Joi.string().required(),
  icon: Joi.number().allow(null).default(null),
  iconkde: Joi.string().allow(null).default(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildTag(db: Database) {
  return class Tag extends ModelBase<TagData> implements TagData {
    constructor(obj: TagData) {
      super(obj, schema);
    }
    get id() {
      return this.raw.id;
    }
    get pid() {
      return this.raw.pid;
    }
    get name() {
      return this.raw.name;
    }
    get icon() {
      return this.raw.icon;
    }
    get iconkde() {
      return this.raw.iconkde;
    }
    static all() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new Tag(result));
    }
    static one(id: number) {
      let result = db.prepare(sql.one).get(id);
      expect(result, messages.error.idNotFound(id)).to.exist;
      return new Tag(result);
    }
  };
}

export default BuildTag;
