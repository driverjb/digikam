import { ObjectSchema } from 'joi';

export abstract class ModelBase<T> {
  public raw: T;
  constructor(obj: T, schema: ObjectSchema<T>) {
    let { error, value } = schema.validate(obj);
    if (error) throw error;
    this.raw = value;
  }
}

export default ModelBase;
