import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../util/messages.module';
import BuildImageComment from './image/ImageComment.class';
import BuildImageCopyright from './image/ImageCopyright.class';
import BuildImageHistory from './image/ImageHistory.class';
import BuildImageInformation from './image/ImageInformation.class';
import BuildImageMetadata from './image/ImageMetadata.class';
import BuildImagePosition from './image/ImagePosition.class';
import BuildImageProperty from './image/ImageProperty.class';
import BuildImageRelation from './image/ImageRelation.class';
import BuildImageTag from './image/ImageTag.class';
import BuildImageTagProperty from './image/ImageTagProperty.class';
import ModelBase from './ModelBase.class';
import BuildTag from './Tag.class';

const sql = {
  all: 'select * from Images',
  one: 'select * from Images where id = ?'
};

export interface ImageData {
  id: number;
  album: number;
  name: string;
  status: number;
  category: number;
  modificationDate: string;
  fileSize: number;
  uniqueHash: string;
  manualOrder: number;
}

const schema = Joi.object<ImageData>({
  id: Joi.number().min(1).required(),
  album: Joi.number().min(1).allow(null).default(null),
  name: Joi.string().required(),
  status: Joi.number().min(1).required(),
  category: Joi.number().min(1).required(),
  modificationDate: Joi.string().required(),
  fileSize: Joi.number().min(1).required(),
  uniqueHash: Joi.string().required(),
  manualOrder: Joi.number().allow(null).default(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImage(db: Database) {
  const ImageComment = BuildImageComment(db);
  const ImageCopyright = BuildImageCopyright(db);
  const ImageHistory = BuildImageHistory(db);
  const ImageInformation = BuildImageInformation(db);
  const ImageMetadata = BuildImageMetadata(db);
  const ImagePosition = BuildImagePosition(db);
  const ImageProperty = BuildImageProperty(db);
  const ImageTag = BuildImageTag(db);
  const ImageTagProperty = BuildImageTagProperty(db);
  const Tag = BuildTag(db);
  return class Image extends ModelBase<ImageData> implements ImageData {
    constructor(obj: ImageData) {
      super(obj, schema);
    }
    get id() {
      return this.raw.id;
    }
    get album() {
      return this.raw.album;
    }
    get name() {
      return this.raw.name;
    }
    get status() {
      return this.raw.status;
    }
    get category() {
      return this.raw.category;
    }
    get modificationDate() {
      return this.raw.modificationDate;
    }
    get fileSize() {
      return this.raw.fileSize;
    }
    get uniqueHash() {
      return this.raw.uniqueHash;
    }
    get manualOrder() {
      return this.raw.manualOrder;
    }
    get comments() {
      return ImageComment.getByImage(this.id);
    }
    get copyrights() {
      return ImageCopyright.getByImage(this.id);
    }
    get history() {
      return ImageHistory.get(this.id);
    }
    get information() {
      return ImageInformation.get(this.id);
    }
    get metadata() {
      return ImageMetadata.get(this.id);
    }
    get position() {
      return ImagePosition.get(this.id);
    }
    get properties() {
      return ImageProperty.getByImage(this.id);
    }
    get relations() {
      throw new Error('Relations not implemented yet');
    }
    get tags() {
      console.log(ImageTag.getByImage(this.id));
      return ImageTag.getByImage(this.id).map((tags) => Tag.one(tags.tagid));
    }
    static all() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new Image(result));
    }
    static one(id: number) {
      let result = db.prepare(sql.one).get(id);
      expect(result, messages.error.idNotFound(id)).to.exist;
      return new Image(result);
    }
    static getByTag(tagids: number[]) {
      return tagids.map((id) => ImageTag.getByTag(id).map((it) => Image.one(it.imageID)))[0];
    }
  };
}

export default BuildImage;
