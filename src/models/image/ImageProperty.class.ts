import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../../util/messages.module';
import { ImageChildBase, ImageChildBaseData } from './ImageChildBase.interface';

const sql = {
  all: 'select * from ImageProperties',
  byImage: 'select * from ImageProperties where imageid = ?'
};

export interface ImagePropertyData extends ImageChildBaseData {
  property: string;
  value: any;
}

const schema = Joi.object<ImagePropertyData>({
  property: Joi.string().required(),
  value: Joi.any().allow(null).default(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImageProperty(db: Database) {
  return class ImageProperty extends ImageChildBase<ImagePropertyData> {
    constructor(obj: ImagePropertyData) {
      super(obj, schema);
    }
    get property() {
      return this.raw.property;
    }
    get value() {
      return this.raw.value;
    }
    static getAll() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new ImageProperty(result));
    }
    static getByImage(imageid: number) {
      return db
        .prepare(sql.byImage)
        .all(imageid)
        .map((result) => new ImageProperty(result));
    }
  };
}

export default BuildImageProperty;

//get $1(){ return this.raw.$1 }
