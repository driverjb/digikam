import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../../util/messages.module';
import { ImageChildBase, ImageChildBaseData } from './ImageChildBase.interface';

const sql = {
  all: 'select * from ImageMetadata',
  one: 'select * from ImageMetadata where imageid = ?'
};

export interface ImageMetadataData extends ImageChildBaseData {
  make: string;
  model: string;
  lens: string;
  aperture: number;
  focalLength: number;
  focalLength35: number;
  exposureTime: number;
  exposureProgram: number;
  exposureMode: number;
  sensitivity: number;
  flash: number;
  whiteBalance: number;
  whiteBalanceColorTemperature: number;
  meteringMode: number;
  subjectDistance: number;
  subjectDistanceCategory: number;
}

const schema = Joi.object<ImageMetadataData>({
  make: Joi.string().allow(null).default(null),
  model: Joi.string().allow(null).default(null),
  lens: Joi.string().allow(null).default(null),
  aperture: Joi.number().allow(null).default(null),
  focalLength: Joi.number().allow(null).default(null),
  focalLength35: Joi.number().allow(null).default(null),
  exposureTime: Joi.number().allow(null).default(null),
  exposureProgram: Joi.number().allow(null).default(null),
  exposureMode: Joi.number().allow(null).default(null),
  sensitivity: Joi.number().allow(null).default(null),
  flash: Joi.number().allow(null).default(null),
  whiteBalance: Joi.number().allow(null).default(null),
  whiteBalanceColorTemperature: Joi.number().allow(null).default(null),
  meteringMode: Joi.number().allow(null).default(null),
  subjectDistance: Joi.number().allow(null).default(null),
  subjectDistanceCategory: Joi.number().allow(null).default(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImageMetadata(db: Database) {
  return class ImageMetadata extends ImageChildBase<ImageMetadataData> {
    constructor(obj: ImageMetadataData) {
      super(obj, schema);
    }
    get make() {
      return this.raw.make;
    }
    get model() {
      return this.raw.model;
    }
    get lens() {
      return this.raw.lens;
    }
    get aperture() {
      return this.raw.aperture;
    }
    get focalLength() {
      return this.raw.focalLength;
    }
    get focalLength35() {
      return this.raw.focalLength35;
    }
    get exposureTime() {
      return this.raw.exposureTime;
    }
    get exposureProgram() {
      return this.raw.exposureProgram;
    }
    get exposureMode() {
      return this.raw.exposureMode;
    }
    get sensitivity() {
      return this.raw.sensitivity;
    }
    get flash() {
      return this.raw.flash;
    }
    get whiteBalance() {
      return this.raw.whiteBalance;
    }
    get whiteBalanceColorTemperature() {
      return this.raw.whiteBalanceColorTemperature;
    }
    get meteringMode() {
      return this.raw.meteringMode;
    }
    get subjectDistance() {
      return this.raw.subjectDistance;
    }
    get subjectDistanceCategory() {
      return this.raw.subjectDistanceCategory;
    }
    static getAll() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new ImageMetadata(result));
    }
    static get(imageid: number) {
      let result = db.prepare(sql.one).get(imageid);
      expect(result, messages.error.idNotFound(imageid)).to.exist;
      return new ImageMetadata(result);
    }
  };
}

export default BuildImageMetadata;
