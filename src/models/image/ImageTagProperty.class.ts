import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../../util/messages.module';
import { ImageChildBase, ImageChildBaseData } from './ImageChildBase.interface';

const sql = {
  all: 'select * from ImageTagProperties',
  one: 'select * from ImageTagProperties where tagid = ?',
  byImage: 'select * from ImageTagProperties where imageid = ?'
};

export interface ImageTagPropertyData extends ImageChildBaseData {
  tagid: number;
  property: string;
  value: any;
}

const schema = Joi.object<ImageTagPropertyData>({
  tagid: Joi.number().min(1).required(),
  property: Joi.string().required(),
  value: Joi.any().allow(null).default(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImageTagProperty(db: Database) {
  return class ImageTagProperty extends ImageChildBase<ImageTagPropertyData> {
    constructor(obj: ImageTagPropertyData) {
      super(obj, schema);
    }
    get tagid() {
      return this.raw.tagid;
    }
    get property() {
      return this.raw.property;
    }
    get value() {
      return this.raw.value;
    }
    static getAll() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new ImageTagProperty(result));
    }
    static get(tagid: number) {
      let result = db.prepare(sql.one).get(tagid);
      expect(result, messages.error.idNotFound(tagid)).to.exist;
      return new ImageTagProperty(result);
    }
    static getByImage(imageid: number) {
      return db
        .prepare(sql.all)
        .all(imageid)
        .map((result) => new ImageTagProperty(result));
    }
  };
}

export default BuildImageTagProperty;
