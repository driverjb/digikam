import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../../util/messages.module';
import { ImageChildBase, ImageChildBaseData } from './ImageChildBase.interface';

const sql = {
  all: 'select * from ImagePositions',
  one: 'select * from ImagePositions where imageid = ?'
};

export interface ImagePositionData extends ImageChildBaseData {
  latitude: string;
  latitudeNumber: number;
  longitude: string;
  longitudeNumber: number;
  altitude: number;
  orientation: number;
  tilt: number;
  roll: number;
  accuracy: number;
  description: string;
}

const schema = Joi.object<ImagePositionData>({
  latitude: Joi.string().allow(null).default(null),
  latitudeNumber: Joi.number().allow(null).default(null),
  longitude: Joi.string().allow(null).default(null),
  longitudeNumber: Joi.number().allow(null).default(null),
  altitude: Joi.number().allow(null).default(null),
  orientation: Joi.number().allow(null).default(null),
  tilt: Joi.number().allow(null).default(null),
  roll: Joi.number().allow(null).default(null),
  accuracy: Joi.number().allow(null).default(null),
  description: Joi.string().allow(null).default(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImagePosition(db: Database) {
  return class ImagePosition extends ImageChildBase<ImagePositionData> {
    constructor(obj: ImagePositionData) {
      super(obj, schema);
    }
    get latitude() {
      return this.raw.latitude;
    }
    get latitudeNumber() {
      return this.raw.latitudeNumber;
    }
    get longitude() {
      return this.raw.longitude;
    }
    get longitudeNumber() {
      return this.raw.longitudeNumber;
    }
    get altitude() {
      return this.raw.altitude;
    }
    get orientation() {
      return this.raw.orientation;
    }
    get tilt() {
      return this.raw.tilt;
    }
    get roll() {
      return this.raw.roll;
    }
    get accuracy() {
      return this.raw.accuracy;
    }
    get description() {
      return this.raw.description;
    }
    static getAll() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new ImagePosition(result));
    }
    static get(imageid: number) {
      let result = db.prepare(sql.one).get(imageid);
      expect(result, messages.error.idNotFound(imageid)).to.exist;
      return new ImagePosition(result);
    }
  };
}

export default BuildImagePosition;
