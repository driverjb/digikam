import { Database } from 'better-sqlite3';
import Joi from 'joi';
import { ImageChildBase, ImageChildBaseData } from './ImageChildBase.interface';

const sql = {
  all: 'select * from ImageTags',
  byTag: 'select * from ImageTags where tagid = ?',
  byImage: 'select * from ImageTags where imageid = ?'
};

export interface ImageTagData extends ImageChildBaseData {
  tagid: number;
}

const schema = Joi.object<ImageTagData>({
  tagid: Joi.number().min(1).required()
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImageTag(db: Database) {
  return class ImageTag extends ImageChildBase<ImageTagData> {
    constructor(obj: ImageTagData) {
      super(obj, schema);
    }
    get tagid() {
      return this.raw.tagid;
    }
    static getAll() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new ImageTag(result));
    }
    static getByTag(tagid: number) {
      return db
        .prepare(sql.byTag)
        .all(tagid)
        .map((result) => new ImageTag(result));
    }
    static getByImage(imageid: number) {
      return db
        .prepare(sql.byImage)
        .all(imageid)
        .map((result) => new ImageTag(result));
    }
  };
}

export default BuildImageTag;

//get $1(){ return this.raw.$1 }
