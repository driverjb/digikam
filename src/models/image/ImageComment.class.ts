import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../../util/messages.module';
import { ImageChildBase, ImageChildBaseData } from './ImageChildBase.interface';

const sql = {
  all: 'select * from ImageComments',
  one: 'select * from ImageComments where id = ?',
  byImage: 'select * from ImageComments where imageid = ?'
};

export interface ImageCommentData extends ImageChildBaseData {
  id: number;
  type: number;
  language: string;
  author?: string;
  date?: string;
  comment: string;
}

const schema = Joi.object<ImageCommentData>({
  id: Joi.number().min(1).required(),
  type: Joi.number().min(1).required(),
  language: Joi.string().required(),
  author: Joi.string().allow(null).default(null),
  date: Joi.string().allow(null).default(null),
  comment: Joi.string().allow(null).default(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImageComment(db: Database) {
  return class ImageComment extends ImageChildBase<ImageCommentData> {
    constructor(obj: ImageCommentData) {
      super(obj, schema);
    }
    get id() {
      return this.raw.id;
    }
    get type() {
      return this.raw.type;
    }
    get language() {
      return this.raw.language;
    }
    get author() {
      return this.raw.author;
    }
    get date() {
      return this.raw.date;
    }
    get comment() {
      return this.raw.comment;
    }
    static getAll() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new ImageComment(result));
    }
    static get(id: number) {
      let result = db.prepare(sql.one).get(id);
      expect(result, messages.error.idNotFound(id)).to.exist;
      return new ImageComment(result);
    }
    static getByImage(imageid: number) {
      return db
        .prepare(sql.byImage)
        .all(imageid)
        .map((result) => new ImageComment(result));
    }
  };
}

export default BuildImageComment;
