import Joi from 'joi';
import ModelBase from '../ModelBase.class';

export interface ImageChildBaseData {
  imageid: number;
}

const imageChildBaseSchema = Joi.object({
  imageid: Joi.number().min(1).required()
});

export abstract class ImageChildBase<T extends ImageChildBaseData> extends ModelBase<T> {
  constructor(obj: T, schema: Joi.ObjectSchema<T>) {
    super(obj, schema.concat(imageChildBaseSchema));
  }
  get imageID() {
    return this.raw.imageid;
  }
}
