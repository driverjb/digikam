import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../../util/messages.module';
import { ImageChildBase, ImageChildBaseData } from './ImageChildBase.interface';

const sql = {
  all: 'select * from ImageCopyright',
  one: 'select * from ImageCopyright where id = ?',
  byImage: 'select * from ImageCopyright where imageid = ?'
};

export interface ImageCopyrightData extends ImageChildBaseData {
  id: number;
  property: string;
  value: string;
  extraValue: string;
}

const schema = Joi.object<ImageCopyrightData>({
  id: Joi.number().min(1).required(),
  property: Joi.string().required(),
  value: Joi.string().allow(null).default(null),
  extraValue: Joi.string().allow(null).default(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImageCopyright(db: Database) {
  return class ImageCopyright extends ImageChildBase<ImageCopyrightData> {
    constructor(obj: ImageCopyrightData) {
      super(obj, schema);
    }
    get id() {
      return this.raw.id;
    }
    get property() {
      return this.raw.property;
    }
    get value() {
      return this.raw.value;
    }
    get extraValue() {
      return this.raw.extraValue;
    }
    static getAll() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new ImageCopyright(result));
    }
    static get(id: number) {
      let result = db.prepare(sql.one).get(id);
      expect(result, messages.error.idNotFound(id)).to.exist;
      return new ImageCopyright(result);
    }
    static getByImage(imageid: number) {
      return db
        .prepare(sql.byImage)
        .all(imageid)
        .map((result) => new ImageCopyright(result));
    }
  };
}

export default BuildImageCopyright;
