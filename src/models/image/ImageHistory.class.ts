import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../../util/messages.module';
import { ImageChildBase, ImageChildBaseData } from './ImageChildBase.interface';

const sql = {
  all: 'select * from ImageHistory',
  one: 'select * from ImageHistory where imageid = ?'
};

export interface ImageHistoryData extends ImageChildBaseData {
  uuid: string;
  history: string;
}

const schema = Joi.object<ImageHistoryData>({
  uuid: Joi.string().required(),
  history: Joi.string().allow(null).default(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImageHistory(db: Database) {
  return class ImageHistory extends ImageChildBase<ImageHistoryData> {
    constructor(obj: ImageHistoryData) {
      super(obj, schema);
    }
    get uuid(){
      return this.raw.uuid
    }
    get history(){
      return this.raw.history
    }
    static getAll() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new ImageHistory(result));
    }
    static get(imageID: number) {
      let result = db.prepare(sql.one).get(imageID);
      expect(result, messages.error.idNotFound(imageID)).to.exist;
      return new ImageHistory(result);
    }
  };
}

export default BuildImageHistory;
