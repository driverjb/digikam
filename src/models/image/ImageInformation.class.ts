import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../../util/messages.module';
import { ImageChildBase, ImageChildBaseData } from './ImageChildBase.interface';

const sql = {
  all: 'select * from ImageInformation',
  one: 'select * from ImageInformation where imageid = ?'
};

export interface ImageInformationData extends ImageChildBaseData {
  rating: number;
  creationDate: string;
  digitizationDate: string;
  orientation: number;
  width: number;
  height: number;
  format: string;
  colorDepth: number;
  colorModel: number;
}

const schema = Joi.object<ImageInformationData>({
  rating: Joi.number().min(-1).max(5).required(),
  creationDate: Joi.string().required(),
  digitizationDate: Joi.string().allow(null).default(null),
  orientation: Joi.number().min(0).required(),
  width: Joi.number().min(1).required(),
  height: Joi.number().min(1).required(),
  format: Joi.string().required(),
  colorDepth: Joi.number().min(1).required(),
  colorModel: Joi.number().min(1).required()
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImageInformation(db: Database) {
  return class ImageInformation extends ImageChildBase<ImageInformationData> {
    constructor(obj: ImageInformationData) {
      super(obj, schema);
    }
    get rating() {
      return this.raw.rating;
    }
    get creationDate() {
      return this.raw.creationDate;
    }
    get digitizationDate() {
      return this.raw.digitizationDate;
    }
    get orientation() {
      return this.raw.orientation;
    }
    get width() {
      return this.raw.width;
    }
    get height() {
      return this.raw.height;
    }
    get format() {
      return this.raw.format;
    }
    get colorDepth() {
      return this.raw.colorDepth;
    }
    get colorModel() {
      return this.raw.colorModel;
    }
    static getAll() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new ImageInformation(result));
    }
    static get(imageid: number) {
      let result = db.prepare(sql.one).get(imageid);
      expect(result, messages.error.idNotFound(imageid)).to.exist;
      return new ImageInformation(result);
    }
  };
}

export default BuildImageInformation;
