import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import Joi from 'joi';
import messages from '../../util/messages.module';
import { ImageChildBase, ImageChildBaseData } from './ImageChildBase.interface';

const sql = {
  all: 'select * from ImageRelations'
};

export interface ImageRelationData extends ImageChildBaseData {
  subject: number;
  object: number;
  type: number;
}

const schema = Joi.object<ImageRelationData>({
  subject: Joi.number().allow(null),
  object: Joi.number().allow(null),
  type: Joi.number().allow(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildImageRelation(db: Database) {
  return class ImageRelation extends ImageChildBase<ImageRelationData> {
    constructor(obj: ImageRelationData) {
      super(obj, schema);
    }
    get subject() {
      return this.raw.subject;
    }
    get object() {
      return this.raw.object;
    }
    get type() {
      return this.raw.type;
    }
    static getAll() {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new ImageRelation(result));
    }
  };
}

export default BuildImageRelation;
