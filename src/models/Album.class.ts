import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import messages from '../util/messages.module';
import ModelBase from './ModelBase.class';
import Joi from 'joi';

const sql = {
  all: `select * from Albums`,
  one: `select * from Albums where id = ?`
};

export interface AlbumData {
  id: number;
  albumRoot: number;
  relativePath: string;
  date: string;
  caption: string;
  collection: string;
  icon: number;
}

const schema = Joi.object<AlbumData>({
  id: Joi.number().min(1).required(),
  albumRoot: Joi.number().min(1).required(),
  relativePath: Joi.string().required(),
  date: Joi.string().required(),
  caption: Joi.string().allow(null).default(null),
  collection: Joi.string().allow(null).default(null),
  icon: Joi.number().allow(null).default(null)
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildAlbum(db: Database) {
  return class Album extends ModelBase<AlbumData> implements AlbumData {
    constructor(obj: AlbumData) {
      super(obj, schema);
    }
    get id() {
      return this.raw.id;
    }
    get albumRoot() {
      return this.raw.albumRoot;
    }
    get relativePath() {
      return this.raw.relativePath;
    }
    get date() {
      return this.raw.date;
    }
    get caption() {
      return this.raw.caption;
    }
    get collection() {
      return this.raw.collection;
    }
    get icon() {
      return this.raw.icon;
    }
    static getAll(): Album[] {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new Album(result));
    }
    static get(id: number): Album {
      let result = db.prepare(sql.one).get(id);
      expect(result, messages.error.idNotFound(id)).to.exist;
      return new Album(result);
    }
  };
}

export default BuildAlbum;
