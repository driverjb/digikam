import { Database } from 'better-sqlite3';
import { expect } from 'chai';
import messages from '../util/messages.module';
import ModelBase from './ModelBase.class';
import Joi from 'joi';

const sql = {
  all: `select * from AlbumRoots`,
  one: `select * from AlbumRoots where id = ?`
};

export interface AlbumRootData {
  id: number;
  label: string;
  status: number;
  type: number;
  identifier: string;
  specificPath: string;
}

const schema = Joi.object<AlbumRootData>({
  id: Joi.number().required().min(1),
  label: Joi.string().required(),
  status: Joi.number().min(0).required(),
  type: Joi.number().min(0).required(),
  identifier: Joi.string().required(),
  specificPath: Joi.string().required()
});

/**
 * Generate the AlbumRoot model with the given database instance
 * @param db Sqlite instance that the model will be applied to
 */
export function BuildAlbumRoot(db: Database) {
  return class AlbumRoot extends ModelBase<AlbumRootData> implements AlbumRootData {
    constructor(obj: AlbumRootData) {
      super(obj, schema);
    }
    get id() {
      return this.raw.id;
    }
    get label() {
      return this.raw.label;
    }
    get status() {
      return this.raw.status;
    }
    get type() {
      return this.raw.type;
    }
    get identifier() {
      return this.raw.identifier;
    }
    get specificPath() {
      return this.raw.specificPath;
    }
    static getAll(): AlbumRoot[] {
      return db
        .prepare(sql.all)
        .all()
        .map((result) => new AlbumRoot(result));
    }
    static get(id: number): AlbumRoot {
      let result = db.prepare(sql.one).get(id);
      expect(result, messages.error.idNotFound(id)).to.exist;
      return new AlbumRoot(result);
    }
  };
}

export default BuildAlbumRoot;
