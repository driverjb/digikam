export const messages = {
  error: {
    idNotFound: (id: number) => `Provided id [${id}] was not found`
  }
};
export default messages;
